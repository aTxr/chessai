CC = g++
_DEBUG = $(if $(DEBUG),-D DEBUG,)
_OPT = $(if $(OPT),-O3 -flto,)
CFLAGS = -g -std=c++2a -Wall $(_OPT) -I./include $(_DEBUG)

.PHONY: clean doc check-syntax compile-all

# rule to generate the doxygen documentation
doc:
	doxygen conf/doxygen.conf

# rule to remove all .o files and all executables
clean:
	- rm -f *.o
	- find . -executable -type f -delete

# compile rules
%.o: ./src/%.cpp
	$(CC) $(CFLAGS) -o $@ -c $^

check-syntax: main.o chessboard.o tree.o

# build rules
main: main.o chessboard.o tree.o
	$(CC) $(CFLAGS) -o $@ $^

compile-all: main
