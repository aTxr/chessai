#pragma once
#include "tree.h"
#include <array>
#include <iostream>

using namespace std;

class Pieces_array {
    array<array<int,16>, 2> pieces_array;
  public:
    Pieces_array();
    Pieces_array(bool white);
    void set_array(int id, coord new_coord);
    int mem(coord x);
};

class Chessboard {
    Pieces_array white;
    Pieces_array black;
  public:
    Chessboard();
    Chessboard(Pieces_array white, Pieces_array black);
    Pieces_array get_white();
    Pieces_array get_black();
    void play_move(Move todo);
};

