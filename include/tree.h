#pragma once
#include <array>
#include <iostream>

using namespace std;

typedef array<int, 2> coord;
class ListMoves;

class Move {
    int id;
    coord next_pos;
  public:
    Move(int id, coord next_pos);
    int get_id();
    coord get_next_pos();
};

// The leaf class for the tree 
class CellMove {
    int score;
    Move cur_move;
    CellMove * next;
  public:
    void set_next(CellMove new_move);
};

class ListMoves {
    CellMove * p_cell;
  public:
    ListMoves();
    bool is_empty();
    CellMove get_cell();
    void add(CellMove new_cell);
};

class Leaf {
    ListMoves * children;
    CellMove root;
  public:
    Leaf(CellMove new_cell);
    void add_child(CellMove new_cell);
};

class Tree {
    Leaf * p_leaf;
public:
    Tree();
    bool is_empty();
    Leaf get_leaf();
    void add_move(CellMove new_cell);
    int minimax(int deep);
};

