#include "../include/chessboard.h"

Pieces_array::Pieces_array(bool white)
{
  if (white) {
    for (int k =0; k<16; k++)
    {
      pieces_array[k][0] = k/8;
      pieces_array[k][1] = k%8;
    }
  }
  else {
    for (int k =0; k<16; k++)
    {
      pieces_array[k][0] = 7 - k/8;
      pieces_array[k][1] = k%8;
    }
  }
}

void Pieces_array::set_array(int id, coord new_coord)
{
  pieces_array[id][0] = new_coord[0];
  pieces_array[id][1] = new_coord[1];
}

int Pieces_array::mem(coord x)
{
  for (int k =0; k<16; k++) {
    if (pieces_array[k][0] == x[0] && pieces_array[k][1] == x[1])
    {
      return k;
    }
  }
  return -1;
}

Chessboard::Chessboard()
  : white(Pieces_array(true))
  , black(Pieces_array(false))
{}

Chessboard::Chessboard(Pieces_array w, Pieces_array b)
  : white(w)
  , black(b)
{}

Pieces_array Chessboard::get_white() { return white; }
Pieces_array Chessboard::get_black() { return black; }

void Chessboard::play_move(Move todo){
  // Move the piece that have to move
  int id = todo.get_id();
  coord next_pos = todo.get_next_pos();
  
  Pieces_array own_pieces = white;
  Pieces_array ennemy_pieces = black;
  if (id >= 16)
  {
    own_pieces = black;
    ennemy_pieces = white;
  }

  own_pieces.set_array(id%16, next_pos);

  // Eat the ennemy piece if there is one 
  int id_ennemy = ennemy_pieces.mem(next_pos);
  if (id_ennemy >= 0)
  {
    ennemy_pieces.set_array(id_ennemy, {-1, -1});
  }
}
