#include "../include/tree.h"
#include <assert.h>
#include <iostream>

using namespace std;

// Move functions
int Move::get_id() { return id; }
coord Move::get_next_pos() { return next_pos; }


// CellMove functions
void CellMove::set_next(CellMove new_cell)
{
  next = &new_cell;
}


// ListMoves functions
bool ListMoves::is_empty()
{
  return p_cell == nullptr;
}

CellMove ListMoves::get_cell()
{
  assert(!this->is_empty());
  return *(p_cell);
}

void ListMoves::add(CellMove new_cell)
{
  if (this->is_empty())
  {
    p_cell = &new_cell;
  }
  else
  {
    get_cell().set_next(new_cell);      
  }
}


// Leaf functions
Leaf::Leaf(CellMove new_cell)
  : root(new_cell) 
{ children = nullptr; }

void Leaf::add_child(CellMove new_cell)
{
  children->add(new_cell);
}



// Tree functions
bool Tree::is_empty()
{
  return p_leaf == nullptr;
}

Leaf Tree::get_leaf()
{
  assert(!this->is_empty());
  return *(p_leaf);
}

void Tree::add_move(CellMove new_cell)
{
  if (this->is_empty())
  {
    *p_leaf = Leaf(new_cell);
  }
  else
  {
    get_leaf().add_child(new_cell);      
  }
}

int Tree::minimax(int deep)
{
  return 0;
}
